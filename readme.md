# TerraLAB

## Atividade Prática - Sprint 4

##### por Webert Meira Rodrigues

## SPRINT 4

> **Descrição:** 
>
> Este projeto consiste em um backend em Node.js para gerenciar usuários e pontos geográficos associados a esses usuários. Os usuários pertencem a um determinado nicho e podem cadastra e remover pontos geográficos, os quais são definidos por coordenadas de latitude e longitude, além de uma descrição, como "escola" ou "farmácia".
<hr>

> **Funcionalidades Principais:** 
>
> Os usuários podem ser cadastrados e/ou removidos do sistema, podem cadastrar pontos com uma relação de vários para um onde um úsuario pode ter vários pontos geograficos
<hr>

> **Tecnologias Utilizadas:** 
>
> *Node.Js, Express.js, Jest(Testes Unitarios), SQLite*
<hr>

> **Como Usar:** 
>
> ```js
>  npm install
>  npm start - Inicia o App e executa os testes
>
>  Caso queira inciar o app ou os testes separadamente:
>
>  npm run dev - Inicia o App
>  npm test - Executa os testes
> ``` 
<hr>

> **Endpoints da API** 
>
> O Projeto está configurado para rodar na porta 8080 por padrão  
> POST /AdicionarUsuarios/email=exemplo@exemplo.com&name=exemplo: Cria um novo usuário.  
> POST /AdicionarPonto/?latitude=-45.8952&longitude=38.5648&email=exemplo@gmail.com Cria um novo ponto ligado ao email do usúario passado.  
>  
> DELETE /RemoverUsuario/?email=exemplo@exemplo.com: Remove um usuário e todos os seus pontos caso tenha  
> DELETE RemoverPonto/?id=1234-5678-9101-2131&user=exemplo: Remove um ponto do usuário  
<hr>