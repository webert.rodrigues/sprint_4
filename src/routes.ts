import { Request, Response, Router } from 'express';
import { UsersController } from './controllers/UsersController';
import { PointsControllers } from './controllers/PointControllers';


const router = Router();

const usersController = new UsersController();
const pointsController = new PointsControllers();

router.post('/AdicionarUsuario', usersController.create);
router.delete('/RemoverUsuario', usersController.delete);

router.post('/AdicionarPonto', pointsController.create);
router.delete('/RemoverPonto', pointsController.delete);


export default router;