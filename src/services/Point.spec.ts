import { Point } from '../entities/Point';
import { User } from '../entities/User';
import { Repository } from 'typeorm';
import { PointsServices } from './PointsServices';

describe('PointsServices', () => {
    let usersRepository: Repository<User>;
    let pointsRepository: Repository<Point>;
    let pointsServices: PointsServices;

    beforeEach(() => {
        usersRepository = createMockRepository<User>();
        pointsRepository = createMockRepository<Point>();
        pointsServices = new PointsServices(usersRepository, pointsRepository);
    });

    describe('create', () => {
        it('should create a point successfully for an existing user', async () => {

            //Arrange
            const longitude = 10;
            const latitude = 20;
            const email = 'test@example.com';
            const description = 'Hospital'
            const user = { id: '1', email: email, name: 'Test User' };
            const point = { id: '1', latitude: latitude, longitude: longitude, user_id: user.id, user: user };
            
            jest.spyOn(usersRepository, 'findOne').mockResolvedValue(user);
            jest.spyOn(pointsRepository, 'create').mockReturnValue(point);
            jest.spyOn(pointsRepository, 'save').mockResolvedValue(point);

            // Act
            const result = await pointsServices.create(longitude, latitude, description, email);

            //Assert
            expect(usersRepository.findOne).toHaveBeenCalledWith({ email: email });
            expect(pointsRepository.create).toHaveBeenCalledWith({
                latitude: latitude,
                longitude: longitude,
                description: description,
                user_id: user.id
            });
            expect(pointsRepository.save).toHaveBeenCalledWith(point);
            expect(result).toEqual(point);
        });

        it('should throw an error when user does not exist', async () => {

            //Arrange
            const longitude = 10;
            const latitude = 20;
            const email = 'test@example.com';
            const description = 'Hospital'

            jest.spyOn(usersRepository, 'findOne').mockResolvedValue(null);

            //Assert
            await expect(pointsServices.create(longitude, latitude, description, email)).rejects.toThrowError('Usuário não encontrado!');

            //Act
            expect(usersRepository.findOne).toHaveBeenCalledWith({ email: email });
            expect(pointsRepository.create).not.toHaveBeenCalled();
            expect(pointsRepository.save).not.toHaveBeenCalled();
        
        })
    });

    describe('delete', () => {
        it('should delete a point successfully', async () => {

            // Arrange
            const id = '1';
            const name = 'Test User';
            const user = { id: '1', email: 'test@example.com', name: name }
            const point = { id: id, latitude: 10, longitude: 20, user_id: '1', user: user}

            jest.spyOn(pointsRepository, 'findOne').mockResolvedValue(point);
            jest.spyOn(usersRepository, 'findOne').mockResolvedValue(user);

            // Act
            await pointsServices.delete(id, name);

            // Assert
            expect(pointsRepository.findOne).toHaveBeenCalledWith({ id: id });
            expect(usersRepository.findOne).toHaveBeenCalledWith({ name: name });
            expect(pointsRepository.delete).toHaveBeenCalledWith({id: id});
    })

    it('should throw an error when point does not exist', async () => {
            // Arrange
            const id = '1';
            const name = 'Test User';
            const user = { id: '1', email: 'test@example.com', name: name }
            const point = { id: id, latitude: 10, longitude: 20, user_id: '1', user: user}

            jest.spyOn(pointsRepository, 'findOne').mockResolvedValue(null);
            jest.spyOn(usersRepository, 'findOne').mockResolvedValue(user);
            

            // Act and Assert
            
            await expect(pointsServices.delete(id, name)).rejects.toThrowError('Ponto não encontrado!');
            expect(pointsRepository.findOne).toHaveBeenCalledWith({ id: id });
            expect(usersRepository.findOne).toHaveBeenCalledWith({ name: name });
            expect(pointsRepository.delete).not.toHaveBeenCalled();

    })

    it('should throw an error when user does not exist', async () => {
            // Arrange
            const id = '1';
            const name = 'Test User';
            const user = { id: '1', email: 'test@example.com', name: name }
            const point = { id: id, latitude: 10, longitude: 20, user_id: '1', user: user}

            jest.spyOn(pointsRepository, 'findOne').mockResolvedValue(point);
            jest.spyOn(usersRepository, 'findOne').mockResolvedValue(null);
            

            // Act and Assert
            
            await expect(pointsServices.delete(id, name)).rejects.toThrowError('Usuário não encontrado!');
            expect(pointsRepository.findOne).toHaveBeenCalledWith({ id: id });
            expect(usersRepository.findOne).toHaveBeenCalledWith({ name: name });
            expect(pointsRepository.delete).not.toHaveBeenCalled();

    })
})

describe
    

});

function createMockRepository<T>(): Repository<T> {
    return {
        findOne: jest.fn(),
        create: jest.fn(),
        save: jest.fn(),
        delete: jest.fn(),
    } as any;
}
