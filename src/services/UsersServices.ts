import { Repository, getCustomRepository } from "typeorm";
import { User } from "../entities/User";
import { UsersRepository } from "../repositories/UsersRepository";
import { log } from "console";

class UsersService {
    private usersRepository: Repository<User>
    constructor(repository: Repository<User>){
        this.usersRepository = repository
    }

    async create(name: string, email: string){
        
        if (!name || name.length == 0 || !email || email.length == 0) {
            throw new Error("Email e Nome são obrigatórios!")
        }

        const user = await this.usersRepository.findOne({email})

        if(user){
            throw new Error("Usuário já existe!")
        }
        
        const userToCreate = await this.usersRepository.create({
            name: name,
            email: email
        })

        return await this.usersRepository.save(userToCreate)

    }

    async delete(email: string){

        const user = await this.usersRepository.findOne({
            email: email
        })

        if(!user){
            throw new Error("Usuário não existe!")
        }
    
        const deletedUser = await this.usersRepository.remove(user)

        return deletedUser
    }
    
}

export { UsersService }

