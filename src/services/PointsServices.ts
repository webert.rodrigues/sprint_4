import { Repository, getCustomRepository } from "typeorm"

import { Point } from "../entities/Point";
import { User } from "../entities/User";

class PointsServices {
    private usersRepository: Repository<User>;
    private pointsRepository: Repository<Point>;
    constructor(usersRepository: Repository<User>, pointsRepository: Repository<Point>){
        this.usersRepository = usersRepository;
        this.pointsRepository = pointsRepository;
    }

    async create(longitude: number, latitude: number, description: string, email: string){

        const user = await this.usersRepository.findOne({
            email: email
        })

        if (!user){
            throw new Error("Usuário não encontrado!")
        }

        const point = await this.pointsRepository.create({
            latitude: latitude,
            longitude: longitude,
            description: description,
            user_id: user.id
        })

        await this.pointsRepository.save(point)

        return point
    }

    async delete(id: string, name: string){

        const point = await this.pointsRepository.findOne({ 
            id
        })

        const user = await this.usersRepository.findOne({
            name
        })

        if (!user){
            throw new Error("Usuário não encontrado!")
        }

        if(!point){
            throw new Error("Ponto não encontrado!")
        }

        return await this.pointsRepository.delete({
            id: id
        })
    }
}

export { PointsServices }