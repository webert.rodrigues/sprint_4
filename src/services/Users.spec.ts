import { Repository } from "typeorm";
import { Point } from "../entities/Point";
import { User } from "../entities/User";
import { PointsServices } from "./PointsServices";
import { v4 as uuid } from "uuid";
import { UsersService } from "./UsersServices";

describe('UsersService', ()=>{
    let usersRepository: Repository<User>;
    let usersService: UsersService;

    beforeEach(() => {
        usersRepository = new Repository<User>();
        usersService = new UsersService(usersRepository);
    
    })
        describe('create', () => {
            it('should create a user successfully', async () => {
            // Arrange
            const name = 'John Doe';
            const email = 'john.doe@example.com';
            const mockUser = new User();

            jest.spyOn(usersRepository, 'findOne').mockResolvedValue(null); 
            jest.spyOn(usersRepository, 'create').mockReturnValue(mockUser);
            jest.spyOn(usersRepository, 'save').mockResolvedValue(mockUser);

            // Act
            const user = await usersService.create(name, email);

            // Assert
            expect(usersRepository.create).toHaveBeenCalledWith({ 
                name, 
                email 
            });
            expect(usersRepository.save).toHaveBeenCalledWith(expect.any(User));
            expect(user).toBeDefined();
            expect(user).toMatchObject(mockUser);
            })

            it('should throw an error when name is not provided', async () => {
                // Arrange
                const name = '';
                const email = 'john.doe@example.com';

                // Act and Assert
                await expect(usersService.create(name, email)).rejects.toThrowError('Email e Nome são obrigatórios!');
            
            })

            it('should throw an error when email is not provided', async () => {
                // Arrange
                const name = 'John Doe';
                const email = '';

                // Act and Assert
                await expect(usersService.create(name, email)).rejects.toThrowError('Email e Nome são obrigatórios!');
            
            })

            it('should throw an error when user already exists', async () => {
                // Arrange
                const mockUser = new User();
                const name = 'John Doe';
                const email = 'john.doe@example.com';

                // Assert
                jest.spyOn(usersRepository, 'findOne').mockResolvedValue(mockUser);

                //Act
                await expect(usersService.create(name, email)).rejects.toThrowError('Usuário já existe!');
            })
        })
    
        describe('delete', ()=>{
            it('should delete a user successfully', async () => {
                // Arrange
                const email = 'john.doe@example.com';
                const mockUser = new User();

                jest.spyOn(usersRepository, 'findOne').mockResolvedValue(mockUser);
                jest.spyOn(usersRepository, 'remove').mockResolvedValue(mockUser);

                // Act
                const user = await usersService.delete(email);

                // Assert
                expect(usersRepository.findOne).toHaveBeenCalledWith({ 
                    email 
                });
                expect(usersRepository.remove).toHaveBeenCalledWith(expect.any(User));
                expect(user).toBeDefined();
                expect(user).toMatchObject(mockUser);

            });

            it('should throw an error when user does not exist', async () => {
                // Arrange
                const email = 'john.doe@example.com';

                // Assert
                jest.spyOn(usersRepository, 'findOne').mockResolvedValue(null);

                // Act and Assert
                await expect(usersService.delete(email)).rejects.toThrowError('Usuário não existe!');
            
            });
        })
    })