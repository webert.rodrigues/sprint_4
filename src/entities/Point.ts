import { Entity, Column, ManyToOne, JoinColumn, PrimaryColumn } from "typeorm";
import { User } from "./User";
import { v4 as uuid } from "uuid";

@Entity("points")
class Point {
    @PrimaryColumn()
    id: string;

    @Column()
    longitude: number;

    @Column()
    latitude: number;

    @Column({ nullable: true })
    description?: string

    @Column()
    user_id: string

    @ManyToOne(() => User)
    @JoinColumn({ name: "user_id" })
    user: User;

    constructor(){
        if(!this.id){
            this.id = uuid();
        }
    }
}

export { Point };
