import { createConnection } from "typeorm"

createConnection();

import 'reflect-metadata';
import '../database';
