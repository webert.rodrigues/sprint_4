import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class CreatePoint1700021011788 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: "points",
                columns: [
                    {
                        name: "id",
                        type: "uuid",
                        isPrimary: true,
                    },
                    {
                        name: "longitude",
                        type: "double precision",
                    },
                    {
                        name: "latitude",
                        type: "double precision",
                    },
                    {
                        name: "description",
                        type: "varchar",
                        isNullable: true,
                    },
                    {
                        name: "user_id",
                        type: "varchar",
                    },
                ]
            })
        )

        await queryRunner.createForeignKey(
            "points",
            new TableForeignKey({
              columnNames: ["user_id"],
              referencedColumnNames: ["id"],
              referencedTableName: "users",
              onDelete: "CASCADE",
            })
          );
    }

    

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey("points", "FK_points_user_id");
        await queryRunner.dropTable("points");
    }

}
