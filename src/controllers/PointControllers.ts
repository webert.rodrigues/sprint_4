import { Request, Response } from "express";
import { PointsServices } from "../services/PointsServices";
import {  getCustomRepository } from "typeorm";
import { UsersRepository } from "../repositories/UsersRepository";
import { PointRepository } from "../repositories/PointRepository";

class PointsControllers {

    async create(req: Request, res: Response){
        const pointsServices = new PointsServices(getCustomRepository(UsersRepository), getCustomRepository(PointRepository));

        const email: string = req.query.email.toString();
        const description: string = req.body.description.toString();
        
        const longitudeString: string = req.query.longitude.toString();
        const longitude: number = parseFloat(longitudeString)

        const latitudeString = req.query.latitude.toString();
        const latitude: number = parseFloat(latitudeString)

        try {

            const point = await pointsServices.create(longitude, latitude, description, email)
            
            return res.status(200).json({message: "Ponto criado com sucesso!", point: point})
            
        } catch (error) {

            if(error.message === 'Usuário não encontrado!'){
                return res.status(404).json({message: 'Usuário não encontrado!'})
            }else {
                return res.status(500).json({message: "Erro interno do servidor!"})
            }
            
        }
    }

    async delete(req: Request, res: Response){

        const name: string = req.query.user.toString()
        const id_point: string = req.query.id.toString()

        const pointsServices = new PointsServices(getCustomRepository(UsersRepository), getCustomRepository(PointRepository));

        
        try {
            const deletedPoint = await pointsServices.delete(id_point, name)
            
            return res.status(200).json({message: "Ponto deletado com sucesso!", deletedPoint: deletedPoint})
        } catch (error) {
            if(error.message === 'Ponto não encontrado!'){
                return res.status(404).json({message: 'Ponto não encontrado!'})
            }
            else if(error.message === 'Usuário não encontrado!'){
                return res.status(404).json({message: 'Usuário não encontrado!'})
            }
        }

    }
}

export { PointsControllers }