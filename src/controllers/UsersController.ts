import { Request, Response } from "express";
import { UsersService } from "../services/UsersServices";
import { getCustomRepository } from "typeorm";
import { UsersRepository } from "../repositories/UsersRepository";

class UsersController {
    
    async create(req: Request, res: Response){
        
        const nome: string = req.query.nome.toString();
        const email: string = req.query.email.toString();

        const usersService = new UsersService(getCustomRepository(UsersRepository));
        
        try {
            const newUser = await usersService.create(nome, email);

            return res.status(200).json({message: 'Usuário cadastrado com sucesso!', user: newUser}).status(200)

        } catch (error) {
            if(error.message === 'Usuário já existe!'){
                return res.status(409).json({message: 'Usuário já existe!'})
            }
            else if(error.message === 'Email e Nome são obrigatórios!'){
                return res.status(400).json({message: 'Email e Nome são obrigatórios!'})
            }
        }
    }// 200 201 400 409 

    async delete(req: Request, res: Response){
        
        const email: string = req.query.email.toString();

        const usersService = new UsersService(getCustomRepository(UsersRepository));

        try {

            await usersService.delete(email);
    
            return res.status(200).json({message: 'Usuário deletado com sucesso!'})
            
        } catch (error) {
            
            if(error.message == 'Usuário não existe!'){
                return res.status(404).json({message: 'Usuário não existe!'}) 
            } else {
                return res.status(500).json({message: 'Erro interno do servidor!'})
            
            }

        }}
}

export { UsersController }